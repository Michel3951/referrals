
package com.michel3951.scorchedmc.referral.Database;

import com.michel3951.scorchedmc.referral.Support.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Sql {

    public static void createDatabase() {
        String url = "jdbc:sqlite:./plugins/StickyVillagers/referrals.db";

        try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                conn.close();
                createTables();
            }
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }

    }

    private static void createTables() {
        Connection conn = getConnection();

        if (conn == null) {
            return;
        }

        try {
            String query = "CREATE TABLE IF NOT EXISTS codes (uuid TEXT, code TEXT, PRIMARY KEY (uuid, code))";
            Statement stmt = conn.createStatement();

            stmt.execute(query);
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }

        try {
            String query = "CREATE TABLE IF NOT EXISTS code_uses (uuid TEXT, code TEXT, ip TEXT, used_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ); CREATE INDEX IF NOT EXISTS `trade_find` ON `trade_history` (name, enchantment)";
            Statement stmt = conn.createStatement();

            stmt.execute(query);
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }
    }

    public static Connection getConnection() {
        Connection conn = null;

        try {
            // db parameters
            String url = "jdbc:sqlite:./plugins/Referrals/referrals.db";
            // create a connection to the database
            conn = DriverManager.getConnection(url);

            return conn;
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }

        return null;
    }
}
