package com.michel3951.scorchedmc.referral.Commands;

import com.michel3951.scorchedmc.referral.Referral;
import com.michel3951.scorchedmc.referral.Support.Chat;
import com.michel3951.scorchedmc.referral.Support.Logger;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import net.milkbowl.vault.economy.Economy;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import org.apache.commons.lang.RandomStringUtils;

public class ReferralCommand implements CommandExecutor {

    protected Connection conn;

    public ReferralCommand(Connection conn) {
        this.conn = conn;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            System.out.println("Command can only be used by players.");
            return true;
        }

        Player p = (Player) sender;

        if (args.length == 0) {
            this.sendCode(p);
            return true;
        } else {
            String code = args[0];

            if (code.length() != 10) {
                p.sendMessage(Chat.colored("&cReferral codes are 10 characters long."));
                return true;
            }

            String uuid = getCodeOwner(code);

            if (uuid == null) {
                p.sendMessage(Chat.colored("&cThis code does not exist."));
                return true;
            }

            if (p.getUniqueId().toString().equals(uuid)) {
                p.sendMessage(Chat.colored("&cYou cannot use your own referral code."));
                return true;
            }

            if (hasUsedCodeBefore(p)) {
                p.sendMessage(Chat.colored("&cYou have already used a code."));
                return true;
            }

            Economy ec = Referral.getEconomy();
            OfflinePlayer owner = Bukkit.getOfflinePlayer(UUID.fromString(uuid));

            ec.depositPlayer(owner, 1500);
            ec.depositPlayer((OfflinePlayer) p, 200);


            if (owner.isOnline()) {
                ((Player) owner).sendMessage(Chat.colored(String.format("&d%s &7has used your referral code! &d$1500 &7has been deposited into your account.", p.getName())));
            }

            p.sendMessage(Chat.colored("Code activated! You have received &d$200 &7to get started."));

            saveUsage(code, p);
            return true;
        }
    }

    private void sendCode(Player p) {

        String code = getPlayerCode(p);

        if (code == null) {
            p.sendMessage(Chat.colored("Creating your code, please wait"));
            code = createCode(p);
        }

        p.sendMessage(Chat.colored("Your referral code is &d" + code + "&7."));
    }

    private String getPlayerCode(Player p) {
        try {
            String query = "SELECT * FROM `codes` WHERE `uuid` = ?;";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, p.getUniqueId().toString());
            ResultSet r = stmt.executeQuery();

            if (!r.next()) {
                return null;
            }

            return r.getString("code");
        } catch (SQLException ex) {
            Logger.error(ex.toString());
        }

        return null;
    }

    private String getCodeOwner(String code) {
        try {
            String query = "SELECT * FROM `codes` WHERE `code` = ?;";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, code);
            ResultSet r = stmt.executeQuery();

            if (!r.next()) {
                return null;
            }

            return r.getString("uuid");
        } catch (SQLException ex) {
            Logger.error(ex.toString());
        }

        return null;
    }

    private Boolean hasUsedCodeBefore(Player p) {
        try {
            String query = "SELECT * FROM `code_uses` WHERE `uuid` = ? OR `ip` = ?;";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, p.getUniqueId().toString());
            stmt.setString(2, p.getAddress().getHostName());
            ResultSet r = stmt.executeQuery();

            if (!r.next()) {
                return false;
            }

            return true;
        } catch (SQLException ex) {
            Logger.error(ex.toString());
        }

        return true;
    }

    private Boolean saveUsage(String code, Player p) {
        try {
            String query = "INSERT INTO `code_uses` (`code`, `uuid`, `ip`) VALUES (?, ?, ?);";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, code);
            stmt.setString(2, p.getUniqueId().toString());
            stmt.setString(3, p.getAddress().getHostName());
            stmt.executeUpdate();

            return true;
        } catch (SQLException ex) {
            Logger.error(ex.toString());
        }

        return true;
    }

    private String createCode(Player p) {
        try {
            String code = RandomStringUtils.randomAlphanumeric(10).toUpperCase();

            String query = "INSERT INTO `codes` (`uuid`, `code`) VALUES (?, ?);";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, p.getUniqueId().toString());
            stmt.setString(2, code);
            stmt.executeUpdate();

            return code;
        } catch (SQLException ex) {
            Logger.error(ex.toString());
        }

        return null;
    }
}
