package com.michel3951.scorchedmc.referral.Commands;

import com.michel3951.scorchedmc.referral.Referral;
import com.michel3951.scorchedmc.referral.Support.Chat;
import com.michel3951.scorchedmc.referral.Support.Logger;
import net.milkbowl.vault.economy.Economy;
import org.apache.commons.lang.RandomStringUtils;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class UsesCommand implements CommandExecutor {

    protected Connection conn;

    public UsesCommand(Connection conn) {
        this.conn = conn;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0) {
            sender.sendMessage(Chat.colored("&cUsage: /uses <code>"));
            return true;
        } else {
            String code = args[0];

            if (code.length() != 10) {
                sender.sendMessage(Chat.colored("&cReferral codes are 10 characters long."));
                return true;
            }

            String uuid = getCodeOwner(code);

            if (uuid == null) {
                sender.sendMessage(Chat.colored("&cThis code does not exist."));
                return true;
            }

            OfflinePlayer owner = Bukkit.getOfflinePlayer(UUID.fromString(uuid));

            sender.sendMessage(Chat.colored(String.format("Code &d%s &7belongs to &d%s&7.", code, owner.getName())));
            sender.sendMessage(Chat.colored(String.format("It has been used &d%d &7times.", getUses(code))));

            return true;
        }
    }

    private String getCodeOwner(String code) {
        try {
            String query = "SELECT * FROM `codes` WHERE `code` = ?;";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, code);
            ResultSet r = stmt.executeQuery();

            if (!r.next()) {
                return null;
            }

            return r.getString("uuid");
        } catch (SQLException ex) {
            Logger.error(ex.toString());
        }

        return null;
    }

    private int getUses(String code) {
        try {
            String query = "SELECT count(*) as uses FROM `code_uses` WHERE `code` = ?;";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, code);
            ResultSet r = stmt.executeQuery();

            if (!r.next()) {
                return 0;
            }

            return r.getInt("uses");
        } catch (SQLException ex) {
            Logger.error(ex.toString());
        }

        return 0;
    }
}
